﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using BGlobalChallenge.Models;

namespace BGlobalChallenge.Data
{
    public class BGlobalChallengeContext : DbContext
    {
        public BGlobalChallengeContext (DbContextOptions<BGlobalChallengeContext> options)
            : base(options)
        {
        }

        public DbSet<BGlobalChallenge.Models.Vehicle> Vehicle { get; set; }
    }
}
