﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace BGlobalChallenge.Models
{
    public enum Brand
    {
        /* Esta es la forma de realizar esta necesidad manualmente, aunque tambien se podría cumplir de la misma forma que se llevo a cabo la funcionalidad
        de Owners, siempre y cuando se posea una API pública con marcas de autos.
         */
        [Display(Name = "-- Select a car brand --")]
        none,
        Fiat,
        Peugeot,
        Audi,
        [Display(Name = "Mercedes-Benz")]
        Mercedes,
        Chevrolet,
        BMW,
        Renault,
        Nissan,
        Toyota,
        Abarth,
        Dacia,
        Infiniti,
        Kia,
        [Display(Name = "Land Rover")]
        LandRover,
        Opel,
        [Display(Name = "Rolls-Royce")]
        RollsRoyce,
        Subaru,
        Volkswagen,
        [Display(Name = "Alfa Romeo")]
        AlfaRomeo,
        Cadillac,
        Ferrari,
        Isuzu,
        KTM,
        Lexus,
        Mini,
        Seat,
        Suzuki,
        Volvo,
        [Display(Name = "Aston Martin")]
        AstonMartin,
        Catterham,
        Iveco,
        Lotus,
        Mitsubishi,
        Skoda,
        Lamborghini,
        Maserati,
        Morgan,
        Porsche,
        Smart,
        Tesla,
        Bentley,
        Citroen,
        Honda,
        Jeep,
        Lancia,
        Mazda
    }
}
