﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace BGlobalChallenge.Models
{
    public class Vehicle
    {
        public int Id { get; set; }

        [Required]
        [StringLength(8, MinimumLength = 8)]
        public string Patent { get; set; }

        [Required]
        public Brand Brand { get; set; }

        [Required]
        public string Model { get; set; }

        [Required]
        [Range(0, 10)]
        [Display(Name = "Door Quantity")]
        public int DoorQty { get; set; }

        [Required]
        public string Owner { get; set; }

    }
}
