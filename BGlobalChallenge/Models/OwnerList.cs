﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BGlobalChallenge.Models
{
    public class OwnerList
    {
        public List<Owner> Owners;
    }
}
