﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BGlobalChallenge.View_Models
{
    public class OwnerView
    {
        public int Id { get; set; }

        public string FullName { get; set; }
    }
}
