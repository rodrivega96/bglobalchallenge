﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using BGlobalChallenge.Data;
using BGlobalChallenge.Models;
using System.Text.Json;
using System.Net.Http;
using Newtonsoft.Json;
using BGlobalChallenge.View_Models;

namespace BGlobalChallenge.Controllers
{
    public class VehiclesController : Controller
    {
        private readonly BGlobalChallengeContext _context;

        public VehiclesController(BGlobalChallengeContext context)
        {
            _context = context;
        }

        // GET: Vehicles
        public async Task<IActionResult> Index()
        {
            return View(await _context.Vehicle.ToListAsync());
        }

        // GET: Vehicles/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var vehicle = await _context.Vehicle
                .FirstOrDefaultAsync(m => m.Id == id);
            if (vehicle == null)
            {
                return NotFound();
            }

            return View(vehicle);
        }

        // GET: Vehicles/Create
        public async Task<IActionResult> Create()
        {
            List<OwnerView> names = new List<OwnerView>();
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri("https://reqres.in/");
            HttpResponseMessage response = client.GetAsync("api/users?per_page=12&page=1").Result;
            if (response.IsSuccessStatusCode)
            {
                names.Insert(0, new OwnerView { Id = 0, FullName = "-- Select owner --" });
                var jsonString = await response.Content.ReadAsStringAsync();
                Console.WriteLine(jsonString);
                var post = JsonDocument.Parse(jsonString).RootElement.GetProperty("data");
                //var cat = post.RootElement.GetProperty("Object").GetProperty("data").GetString();
                var deserialized = JsonConvert.DeserializeObject<IEnumerable<Owner>>(post.ToString());
                //Console.WriteLine(deserialized);
                int i = 1;
                foreach (var json in deserialized)
                {
                    names.Insert(i, new OwnerView { Id = i, FullName = json.First_Name + " " + json.Last_Name });
                    i++;
                }
            }
            ViewBag.message = names;
            return View();
        }

        // POST: Vehicles/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Patent,Brand,Model,DoorQty,Owner")] Vehicle vehicle)
        {
            // Aqui esta la validacion del lado del servidor.
            if (ModelState.IsValid)
            {
                _context.Add(vehicle);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }

            ViewBag.message = GetOwners();
            return View(vehicle);
        }

        // GET: Vehicles/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var vehicle = await _context.Vehicle.FindAsync(id);
            if (vehicle == null)
            {
                return NotFound();
            }

            List<OwnerView> names = new List<OwnerView>();
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri("https://reqres.in/");
            HttpResponseMessage response = client.GetAsync("api/users?per_page=12&page=1").Result;
            if (response.IsSuccessStatusCode)
            {
                names.Insert(0, new OwnerView { Id = 0, FullName = "-- Select owner --" });
                var jsonString = await response.Content.ReadAsStringAsync();
                Console.WriteLine(jsonString);
                var post = JsonDocument.Parse(jsonString).RootElement.GetProperty("data");
                //var cat = post.RootElement.GetProperty("Object").GetProperty("data").GetString();
                var deserialized = JsonConvert.DeserializeObject<IEnumerable<Owner>>(post.ToString());
                //Console.WriteLine(deserialized);
                int i = 1;
                foreach (var json in deserialized)
                {
                    names.Insert(i, new OwnerView { Id = i, FullName = json.First_Name + " " + json.Last_Name });
                    i++;
                }
            }
            ViewBag.message = names;
            return View(vehicle);
        }

        // POST: Vehicles/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Patent,Brand,Model,DoorQty,Owner")] Vehicle vehicle)
        {
            if (id != vehicle.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(vehicle);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!VehicleExists(vehicle.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }

            ViewBag.message = GetOwners();
            return View(vehicle);
        }

        // GET: Vehicles/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var vehicle = await _context.Vehicle
                .FirstOrDefaultAsync(m => m.Id == id);
            if (vehicle == null)
            {
                return NotFound();
            }

            return View(vehicle);
        }

        // POST: Vehicles/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var vehicle = await _context.Vehicle.FindAsync(id);
            _context.Vehicle.Remove(vehicle);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool VehicleExists(int id)
        {
            return _context.Vehicle.Any(e => e.Id == id);
        }

        // Método para obtener el listado de los "dueños" de la api ofrecida.
        private async Task<List<OwnerView>> GetOwners()
        {
            List<OwnerView> names = new List<OwnerView>();
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri("https://reqres.in/");
            HttpResponseMessage response = client.GetAsync("api/users?per_page=12&page=1").Result;
            if (response.IsSuccessStatusCode)
            {
                names.Insert(0, new OwnerView { Id = 0, FullName = "-- Select owner --" });
                var jsonString = await response.Content.ReadAsStringAsync();
                Console.WriteLine(jsonString);
                var post = JsonDocument.Parse(jsonString).RootElement.GetProperty("data");
                //var cat = post.RootElement.GetProperty("Object").GetProperty("data").GetString();
                var deserialized = JsonConvert.DeserializeObject<IEnumerable<Owner>>(post.ToString());
                //Console.WriteLine(deserialized);
                int i = 1;
                foreach (var json in deserialized)
                {
                    names.Insert(i, new OwnerView { Id = i, FullName = json.First_Name + " " + json.Last_Name });
                    i++;
                }
            }
            return names;
        }
    }
}
